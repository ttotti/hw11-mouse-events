/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
Події в JavaScript - це механізм, який дозволяє реагувати на дії користувача або зміни стану елементів на сторінці. Вони використовуються для обробки подій, таких як клік мишею, натискання клавіші, завантаження сторінки, зміни значення поля вводу та інші.


2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
- click
- mousedown 
- mouseup
- mousemove
- mouseenter 
- mouseleave

3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
   Подія “contextmenu” виникає, коли користувач натискає праву кнопку миші на елементі. Вона дозволяє вам відобразити контекстне меню зі спеціальними опціями для цього елемента. 

Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
 
 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
  По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */

//   btn-click

const btnClick = document.querySelector("#btn-click");

const sectionContent2 = document.getElementById("content");

function createNewParagraph() {
  const newParagraph = document.createElement("p");
  newParagraph.textContent = "New paragraph*";
  sectionContent2.append(newParagraph);
}

btnClick.onclick = createNewParagraph;

// _________________________________________________

const btnCreate = document.createElement("button");
btnCreate.id = "btn-input-create";
btnCreate.style.display = "block";
btnCreate.style.backgroundColor = "yellow";
btnCreate.style.margin = `0 0 4px 4px`;
btnCreate.style.padding = `5px 15px`;
btnCreate.textContent = "Create input";

const sectionContent = document.getElementById("content");

sectionContent.parentNode.insertBefore(btnCreate, sectionContent.nextSibling);

btnCreate.addEventListener("click", function () {
  const newInput = document.createElement("input");
  newInput.type = "text";
  newInput.placeholder = "Enter a value";
  newInput.name = "newInput";
  newInput.style.padding = `5px 15px`;
  newInput.style.margin = `3px`;

  sectionContent.parentNode.insertBefore(newInput, btnCreate.nextSibling);
});
